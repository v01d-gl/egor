run:
	uv run python src/main.py

lint:
	uv run ruff check

lintfix: format
	uv run ruff check --fix

format:
	uv run ruff format

build: clean-build
	uv run pyinstaller -Fwn egor -p src src/main.py

clean-build: clean-build-config
	rm -rf build
	rm -rf dist

clean-build-config:
	rm -f egor.spec
