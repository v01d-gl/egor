from sys import argv, exit
from threading import Thread
from time import sleep

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QColor, QFont, QPainter
from PyQt6.QtWidgets import QApplication, QWidget

from config import settings


BYPASSES = (
    Qt.WindowType.FramelessWindowHint
    | Qt.WindowType.WindowStaysOnTopHint
    | Qt.WindowType.Tool
    | Qt.WindowType.X11BypassWindowManagerHint
    | Qt.WindowType.BypassWindowManagerHint
)


class Window(QWidget):
    def __init__(self):
        super().__init__()

        self.text_color = list(settings.init_text_color)
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        screen = QApplication.primaryScreen()

        if screen is None:
            raise RuntimeError("Failed to get primary screen")

        geom = screen.geometry()
        self.setFixedSize(geom.width(), geom.height())
        self.move(0, 0)
        self.setWindowFlags(BYPASSES)
        self.show()

    def paintEvent(self, a0):  # noqa: N802
        qp = QPainter()
        qp.begin(self)
        self.draw_text(a0, qp)
        qp.end()

    def draw_text(self, event, qp):
        qp.setPen(QColor(*self.text_color))
        qp.setFont(QFont(settings.font_type, settings.font_size))
        qp.drawText(event.rect(), Qt.AlignmentFlag.AlignCenter, settings.text)

    def enable_gradient(self):
        gradient_coef_list = list(settings.gradient_coef)

        while True:
            for i, (current_color, gradient_coef) in enumerate(zip(self.text_color, gradient_coef_list, strict=True)):
                possible_color = current_color + gradient_coef
                if possible_color > 255 or possible_color < 0:
                    gradient_coef_list[i] = -gradient_coef_list[i]

                self.text_color[i] = gradient_coef_list[i] + self.text_color[i]

            self.repaint()
            sleep(settings.render_delay)


if __name__ == "__main__":
    app = QApplication(argv)
    win = Window()

    sleep(settings.show_delay)

    if settings.gradient:
        grad = Thread(target=win.enable_gradient)
        grad.start()

    exit(app.exec())
