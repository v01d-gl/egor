from pydantic_settings import (
    BaseSettings,
    CliImplicitFlag,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
    TomlConfigSettingsSource,
)


class Settings(BaseSettings):
    model_config = SettingsConfigDict(cli_parse_args=True, cli_kebab_case=True)

    init_text_color: tuple[int, int, int] = (8, 5, 10)
    gradient_coef: tuple[int, int, int] = (7, 11, 4)
    text: str = "Вечность пахнет нефтью!"
    font_type: str = "Gabriola"
    font_size: int = 128
    render_delay: float = 0.045
    show_delay: float = 0
    gradient: CliImplicitFlag[bool] = False

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            init_settings,
            env_settings,
            dotenv_settings,
            file_secret_settings,
            TomlConfigSettingsSource(settings_cls, "egor.toml"),
        )


settings = Settings()  # type: ignore
