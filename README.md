# Egor — Text Overlay Printer

## How To Run

### Prepare your environment (once)

Install [uv](https://docs.astral.sh/uv/)

Using scoop

```bash
scoop install uv
```

Without scoop

```ps1
powershell -ExecutionPolicy ByPass -c "irm https://astral.sh/uv/install.ps1 | iex"
```

## Configuration

If `./egor.toml` exists, app will try to load config from this file.

See CLI help message

```text
usage: main.py [-h] [--init-text-color tuple[int,int,int]] [--gradient-coef tuple[int,int,int]] [--text str]
               [--font-type str] [--font-size int] [--render-delay float] [--show-delay float]
               [--gradient | --no-gradient]

options:
  -h, --help            show this help message and exit
  --init-text-color tuple[int,int,int]
                        (default: (8, 5, 10))
  --gradient-coef tuple[int,int,int]
                        (default: (7, 11, 4))
  --text str            (default: Вечность пахнет нефтью!)
  --font-type str       (default: Gabriola)
  --font-size int       (default: 128)
  --render-delay float  (default: 0.045)
  --show-delay float    (default: 0)
  --gradient, --no-gradient
                        (default: False)
```

### Run

#### Using [Make](https://www.gnu.org/software/make/)

```bash
make
```

#### If Make is not installed

```bash
uv run python src/main.py
```

### Build

```ps1
make build
```

## Example with compiled egor.exe

```bash
egor --text "good text" --show-delay 1800
```
